elogind (239.2-1) unstable; urgency=medium

  * Enable tests.

 -- Mark Hindley <mark@hindley.org.uk>  Fri, 23 Nov 2018 11:04:04 +0000

elogind (239.1+20181115-1) unstable; urgency=medium

  * Upstream fix to address excessive delay closing sessions.
  * To build polkit, we will need to install libsystemd-dev and libelogind-dev
    simlultaneously. So
     - move dev manpages to separate package.
     - no longer conflict -dev package with libsystemd-dev.
  * Disable Dbus activation by default.
  * Remove upstream pwx git submodule. Thanks to Ian Jackson.
  * Rework packaging for meson buildsystem.
  * Change libpam-elogind Breaks dependencies to Conflicts.
  * Update to debhelper compat 11.
  * Update to Standards version 4.2.1 (no changes).
  * Enable LTO build as it works with binutils from unstable (2.31.1).
  * Move documentation consistency changes to a quilt patch.
  * Add AppStream metainfo.
  * Add upstream watchfile.
  * Don't package /lib/udev/rules.d/70-power-switch.rules which is also in udev.
  * Configure with -Dsplit-usr=true to ensure consistent build.
  * New version of /etc/pam.d/elogind-user based on Debian systemd.
  * New upstream release v239.1.

 -- Mark Hindley <mark@hindley.org.uk>  Fri, 23 Nov 2018 11:01:14 +0000

elogind (234.4-2~exp1) experimental; urgency=medium

  * debian/rules
    - Make libpam-elogind break libpam-ck-connector
  * enable pristine-tar 

 -- Andreas Messer <andi@bastelmap.de>  Thu, 22 Feb 2018 19:49:52 +0100

elogind (234.4-1+devuan1.4) experimental; urgency=medium

  *  debian/libelogind-dev.install
    -  Drop manpages to allow installation in parallel
       with libsystemd-dev
  * debian/extras/elogind
    - enable elogind pam by default

 -- Andreas Messer <andi@bastelmap.de>  Wed, 07 Feb 2018 22:23:46 +0100

elogind (234.4-1+devuan1.3) experimental; urgency=medium

  * debian/patches/fix-forking.diff 
    - Fix startpar not erminating due to open file descriptors after
      forking

 -- Andreas Messer <andi@bastelmap.de>  Sat, 27 Jan 2018 12:44:32 +0100

elogind (234.4-1+devuan1.2) experimental; urgency=medium

  * debian/rules
    - disable killing background processes on logout

 -- Andreas Messer <andi@bastelmap.de>  Sat, 20 Jan 2018 16:53:32 +0100

elogind (234.4-1+devuan1) experimental; urgency=medium

  * Initial release.

 -- Andreas Messer <andi@bastelmap.de>  Thu, 04 Jan 2018 19:26:09 +0100
